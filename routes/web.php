<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('Home');
});
Route::get('/TT', function () {
    return view('Test');
});

Route::get('/Register', function () {
    return view('Register');
});

Route::get('/LG', function () {
    return view('Login');
});

Route::get('/Category', function () {
    return view('Category');
});

Route::get('/Create', function () {
    return view('Create');
});

