<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
  <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js"></script>
  <script src="//code.jquery.com/jquery-1.11.1.min.js"></script>
  <link href='https://fonts.googleapis.com/css?family=Open+Sans+Condensed:300' rel='stylesheet' type='text/css'>
  <style>
    body, html{
      background-color:#34515E;
      font-family: 'Open Sans Condensed', sans-serif;
      font-size: 18px;
    }
    .register-form{
        font-size: 16px;
      left: 50%;
        top: 50%;
        position: absolute;
        -webkit-transform: translate3d(-50%, -50%, 0);
        -moz-transform: translate3d(-50%, -50%, 0);
        transform: translate3d(-50%, -50%, 0);
    }
    .regbutton{    
        height: 50px;
        width: 200px;
        background-color:tomato;
        border-radius: 0px;
        font-size: 18px;
        color:white;
        border: none !important;
        margin-bottom: 5px;
    }
    .regbutton:hover{    
        color: white;
        background-color:#aa422f;
    }
    .regbutton:active{    
        color: white;
        background-color:#aa422f;
    }
    .logbutton{    
        height: 50px;
        width: 200px;
        background-color:yellowgreen;
        border-radius: 0px;
        font-size: 18px;
        color:white;
        border: none !important;
        margin-bottom: 5px;
    }
    .logbutton:hover{    
        color: white;
        background-color:darkolivegreen;
    }
    .logbutton:active{    
        color: white;
        background-color:darkolivegreen;
    }
    .register-form label{
        color: aliceblue;
        
    }
    .register-form input{
        margin-bottom: 5px;
        width: 430px;
        height: 40px;
        border-radius: 0px;
    }
  </style>

</head>
<body>
<div class="container-fluid">
	 <form action="#" method="get" class="register-form"> 
      <div class="row">      
           <div class="col-md-4 col-sm-4 col-lg-4">
              <label for="firstName">Username</label>
               <input name="Username" class="form-control" type="text">    
           </div>            
      </div>
      <div class="row">
           <div class="col-md-4 col-sm-4 col-lg-4">
              <label for="password">Password</label>
               <input name="password" class="form-control" type="password">             
           </div>            
      </div>
      <div class="row">
           <div class="col-md-4 col-sm-4 col-lg-4">
              <label for="name">Nickname</label>
               <input name="name" class="form-control" type="text">             
           </div>            
      </div>
      <hr>
      <div class="row">
        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
        <a class="btn btn-default regbutton" href="/">กลับ</a>
        <!-- <button class="btn btn-default regbutton">กลับ</button> -->
        </div>

        <div class="col-md-6 col-sm-6 col-xs-6 col-lg-6">
        <button class="btn btn-default regbutton" type="submit" name="submit" onclick="login">สมัครใช้งาน</button>
        </div>          
      </div> 
      <center><div class="col-md">
        <a class="btn btn-default regbutton" href="login/">Login</a>
    </div></center>   
    </form>
</div>
<?php
    $servername = "localhost";
    $username = "dbAdmin";
    $password = "P@ss1234";
    $dbname = "projectweb";

    $conn = new mysqli($servername,$username,$password,$dbname);
    if ($conn->connect_error) {
        die("Connection failed: " . $conn->connect_error);
    }else{
        if (isset($_GET['submit'])){
            $U_USER = $_GET['Username'];
            $U_PASS = $_GET['password'];
            $U_NAME = $_GET['name'];

            $sql = "INSERT INTO `USERS`  (`Name_U`, `Password_U`, `U_Name`) 
            VALUES ('$U_USER','$U_PASS','$U_NAME')";
            if ($conn->query($sql) === TRUE) {
                echo "New record created successfully";
            } else {
                echo "Error: " . $sql . "<br>" . $conn->error;  
            }
            $conn->close();
        }else{
            // echo 'กรุณากรอกข้อมูล';
        }
    }
?>
</body>
</html>