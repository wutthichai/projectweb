<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>ร้อยทิป</title>
    <link rel="shortcut icon" href="{{{ asset('img/find_user.png') }}}">
    <link href="{{ asset('vendor\bootstrap\css\bootstrap.min.css') }}" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Saira+Extra+Condensed:500,700" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Muli:400,400i,800,800i" rel="stylesheet">
    <link href="{{ asset('vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet">
    <link href="{{ asset('css/resume.min.css') }}" rel="stylesheet">
    <style>
        .bg {
            background-image: url("img/conn.jpg");
        }

        .bt {
            text-align: center;
            font-size: 20px;
            width: 300px;
            height: 100px;
        }
    </style>
</head>

<body id="page-top">
    <?php
    $color = ["btn btn-primary", "btn btn-secondary", "btn btn-success", "btn btn-info", "btn btn-warning", "btn btn-danger"];
    shuffle($color);
    ?>
    <nav class="navbar navbar-expand-lg navbar-dark bg-primary fixed-top" id="sideNav">
        <a class="navbar-brand js-scroll-trigger" href="#page-top">
            <span class="d-block d-lg-none">พันทิป</span>
            <span class="d-none d-lg-block">
                <img class="img-fluid img-profile rounded-circle mx-auto mb-2" src="img/find_user.png" alt="">
            </span>
        </a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/">หน้าหลัก</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/Category">เลือกหมวดหมู่</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/Create">ตั้งกระทู้</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="#Check">เช็คกระทู้</a>
                </li>
                <!-- <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="">Interests</a>
                </li> -->
                <li class="nav-item">
                    <br><br><br><br><br><br><br><br>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/login">เข้าสู่ระบบ</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link js-scroll-trigger" href="/register">สมัครสมาชิก</a>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container-fluid p-0">
        <hr class="m-0">
        <!-- Category -->
        <section class="resume-section p-3 p-lg-5 d-flex justify-content-center bg" id="Category">
            <div class="w-100">
                <center>
                    <h1>หมวดหมู่</h1><br><br><br>
                    <div>
                        <button type="button" id="sport" class="col-3 bt <?php print_r($color[0]); ?>">กีฬา</button>
                        <button type="button" id="cartoon" class="col-3 bt <?php print_r($color[1]); ?>">การ์ตูน</button>
                        <button type="button" id="love" class="col-3 bt <?php print_r($color[2]); ?>">ความรัก</button>
                        <button type="button" id="it" class="col-3 bt <?php print_r($color[3]); ?>">ไอที</button>
                        <button type="button" id="paryut" class="col-3 bt <?php print_r($color[4]); ?>">การเมือง</button>
                        <button type="button" id="food" class="col-3 bt <?php print_r($color[3]); ?>">อาหาร</button>
                        <button type="button" id="animal" class="col-3 bt <?php print_r($color[2]); ?>">สัตว์</button>
                        <button type="button" id="music" class="col-3 bt <?php print_r($color[1]); ?>">เพลง</button>
                        <button type="button" id="movie" class="col-3 bt <?php print_r($color[0]); ?>">ภาพยนตร์</button>
                    </div>
                </center>
            </div>

        </section>
    </div>

    <!-- Bootstrap core JavaScript -->
    <script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>
    <script src="{{ asset('js/resume.min.js') }}"></script>
</body>


</html> 