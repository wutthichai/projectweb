<!DOCTYPE html>
<html lang="en">
<head>
		<meta charset="UTF-8">
		<!-- <link rel="stylesheet" href="style.css"> -->
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
		<style>
			*{
				font-family: "montserrat",sans-serif;
			}
			.login-box{
				position: absolute;
				top: 0;
				left: 0;
				width: 100%;
				height: 100vh;
				background-image: linear-gradient(45deg,#9fbaa8,#31354c);
				transition: 0.4s;
			}
			.login-form{
				position: absolute;
				top: 50%;
				left: 50%;
				transform: translate(-50%,-50%);
				color: white;
				text-align: center;
			}
			.login-form h1{
				font-weight: 400;
				margin-top: 0;
			}
			.txtb{
				display: block;
				box-sizing: border-box;
				width: 240px;
				background: #ffffff28;
				border: 1px solid white;
				padding: 10px 20px;
				color: white;
				outline: none;
				margin: 10px 0;
				border-radius: 6px;
				text-align: center;
			}
			.login-btn{
				width: 240px;
				background: #2c3e50;
				border: 0;
				color: white;
				padding: 10px;
				border-radius: 6px;
				cursor: pointer;
			}
		</style>
</head>
<body>
<div class="login-box">
	<form class="login-form" action="#" method="get">
		<h1>Welcome</h1>
		<input class="txtb" type="text" name="Username" placeholder="Username">
		<input class="txtb" type="password" name="password" placeholder="Password">
		<input class="login-btn" type="submit" name="submit" value="submit">
		<p></p>
	</form>
</div>
<?php
	$servername = "localhost";
	$username = "dbAdmin";
	$password = "P@ss1234";
	$dbname = "projectweb";
	
	$conn = mysqli_connect($servername, $username, $password, $dbname);
	if (isset($_GET['submit'])){
		$users = $_GET['Username'];
		$Passs = $_GET['password'];
		$sql = "SELECT * FROM `USERS`";
		$result = mysqli_query($conn, $sql);
		$a=array();
		
		if (mysqli_num_rows($result) > 0) {
			while($row = mysqli_fetch_array($result, MYSQLI_BOTH)) {
				$a[$row[1]]=$row[2];
			}
			if (array_key_exists($users,$a)){
				if($Passs==$a[$users]){
					print("<script language='javascript'>alert(\"สวัสดี \ $users \\ยินดีต้อนรับสู่เว็บไซต์ ร้อยทิป\");</script>");
					print("<script language='javascript'>window.location = '/';</script>");
				}else{
					print("<script language='javascript'>alert(\"พาสเวิร์ดของ \ $users \\ไม่ถูกต้อง\");</script>");
				}
			}
			else{
				print("<script language='javascript'>alert(\"ไม่มีผู้ใช้ \ $users \\ในระบบนี้\");</script>");
				
			}
		} else {
			echo "0 results";
		}}
	mysqli_close($conn);
?>
</body>
</html>